<?php
/*
	Ruft die UUID einer OpenSim Profile Textur ab und gibt deren UUID aus.
	IN: GET = HomeURI
	IN: GET = AvatarUUID
	OUT: TexturUUID
*/

function gen_uuid() 
	{
		$uuid = array('time_low'  => 0, 'time_mid'  => 0, 'time_hi'  => 0, 'clock_seq_hi' => 0, 'clock_seq_low' => 0, 'node' => array());
		$uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
		$uuid['time_mid'] = mt_rand(0, 0xffff);
		$uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
		$uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
		$uuid['clock_seq_low'] = mt_rand(0, 255);
		
		for ($i = 0; $i < 6; $i++) 
		{
			$uuid['node'][$i] = mt_rand(0, 255);
		}
		
		$uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
		$uuid['time_low'],
		$uuid['time_mid'],
		$uuid['time_hi'],
		$uuid['clock_seq_hi'],
		$uuid['clock_seq_low'],
		$uuid['node'][0],
		$uuid['node'][1],
		$uuid['node'][2],
		$uuid['node'][3],
		$uuid['node'][4],
		$uuid['node'][5]
		);

		return $uuid;
	}

function getProfileURL($_loginURL, $_ownerUUID)
	{
		$_options 		= 	array('http' => array('http' => array('timeout' => 2), 'method'  => 'POST', 'header'  => 'Content-type: application/xml', 'content' => '<?xml version="1.0" encoding="utf-8"?><methodCall><methodName>get_server_urls</methodName><params><param><value><struct><member><name>userID</name><value><string>'.$_ownerUUID.'</string></value></member></struct></value></param></params></methodCall>'));
		$_xmlContend 	= 	file_get_contents($_loginURL, false, stream_context_create($_options));

		$_xmlData 		=	new SimpleXMLElement($_xmlContend);

		foreach($_xmlData->params->param->value->struct->member as $value)
		{
			if($value->name == "SRV_ProfileServerURI")
			{
				$returnURL 	= $value->value->string[0].""; 
				
				$returnURL	=	str_replace("https://", "", $returnURL);
				$returnURL	=	str_replace("http://", "", $returnURL);
				$returnURL	=	str_replace("/", "", $returnURL);
				
				return "http://".$returnURL;
			}
		}
		
		return "";
	}
	
function getProfileImage($_profileUUID, $_userUUID)
	{
		$_options 	= 	array('http' => array('http' => array('timeout' => 2), 'method'  => 'POST', 'header'  => 'Content-type: application/json-rpc', 'content' => '{"jsonrpc":"2.0","id":"'.gen_uuid().'","method":"avatar_properties_request","params":{"UserId":"'.$_userUUID.'"}}'));
		$_xmlData 	= 	file_get_contents($_profileUUID, false, stream_context_create($_options), null, 166);
		$_uuid 		= 	explode("\"ImageId\":\"",$_xmlData,2);
		
		return $_uuid[1];
	}
	
$m_url 			= 	getProfileURL(@$_GET['HomeURI'], @$_GET['AvatarUUID']);
$m_texturUUID	=	getProfileImage($m_url, @$_GET['AvatarUUID']);

echo $m_texturUUID;
?>