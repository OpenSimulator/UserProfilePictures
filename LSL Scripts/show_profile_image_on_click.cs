key     m_http_request_key_profile_url  =   NULL_KEY;
key     m_http_request_key_image_uuid   =   NULL_KEY;

getProfileServer(string _homeURI, key _user)
    {
        string request = "<?xml version=\"1.0\" encoding=\"utf-8\"?><methodCall><methodName>get_server_urls</methodName><params><param><value><struct><member><name>userID</name><value><string>"+ _user +"</string></value></member></struct></value></param></params></methodCall>";
        m_http_request_key_profile_url = llHTTPRequest(_homeURI, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/xml"], request);
    }

getProfileImage(string _profilServer, key _user)
    {
        string request = "{\"jsonrpc\":\"2.0\",\"id\":\"" + llGenerateKey() + "\",\"method\":\"avatar_properties_request\",\"params\":{\"UserId\":\"" + _user + "\"}}";
        m_http_request_key_image_uuid = llHTTPRequest(_profilServer, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/json-rpc"], request);
    }
    
key m_lastUser		=	NULL_KEY;
    
default
{
    touch_start(integer i)
    {
        m_lastUser = llDetectedKey(0);
        getProfileServer(osGetAvatarHomeURI(m_lastUser), m_lastUser);
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        if (request_id == m_http_request_key_profile_url)
        {
            list _serverData = llParseString2List(body, ["><", ">", "<"], []);
            integer _ll = llGetListLength(_serverData);
            
            integer index = 0;
            while (index < _ll)
            {
                if(llList2String(_serverData, index) == "SRV_ProfileServerURI")
                {
                    getProfileImage(llList2String(_serverData, index + 4), m_lastUser);
                }
                index++;
            }
        }
        
        if (request_id == m_http_request_key_image_uuid)
        {
        
            list _serverData = llParseString2List(body, ["\":\"", "\",\""], []);
            integer _ll = llGetListLength(_serverData);
            
            integer index = 0;
            while (index < _ll)
            {
                if(llList2String(_serverData, index) == "ImageId")
                {
                    llSetTexture(llList2String(_serverData, index + 1), ALL_SIDES);
                }
                index++;
            }
        }  
    }
} 